from os import listdir, path, makedirs


def get_ort_files(ort_dir):
    """
    :param ort_dir: path till folder med ort-filer
    :return: lista med path till alla ort filer
    """
    # Kolla om folder finns annars skapa den
    if not path.isdir(ort_dir):
        makedirs(ort_dir)
    # lista alla filer i folder och returnera lista med path + filnamn för varje fil
    return [ort_dir + "/" + file for file in listdir(ort_dir)]


def create_config_file(template_file, ort_file):
    """
    Skapar en config-fil från template-fil och värde fil
    :param ort_file: path till ort specifik fil
    :return: Konfigurationssträng
    """
    # Läs in template-fil och ersätt variabler med värden

    # Kontroll att filen existerar, finns den inte return None (error)
    if not path.isfile(template_file):
        print(f"[ERROR] File: {template_file} does not exists")
        return None

    with open(ort_file) as fp:
        data = fp.readlines()

    # Skapa dictionary med key, value pairs från ort specifik fil
    ort_values = dict()
    for line in data:
        try:
            key = line.split("=")[0]
            val = line.split("=")[1]
        except Exception as e:
            print(f"[ERROR]: {e}")
            print(f"check syntax of file: {ort_file}")
            return None

        ort_values[key] = val

    with open(template_file) as fp:
        data = fp.read()
    for k,v in ort_values.items():
        data = data.replace("{" + k + "}", v.rstrip())

    return data


if __name__ == "__main__":
    TEMPLATE_FILE = "./envs.txt"
    ORT_FOLDER = "./orter"
    CONF_FOLDER = "./confs"
    # Hämta lista med relativ-path till varje ort-fil
    orter = get_ort_files(ORT_FOLDER)
    # Kolla att CONF_FOLDER finns annars skapa den
    if not path.isdir(CONF_FOLDER):
        makedirs(CONF_FOLDER)
    # Gå igenom varje ort-fil och skapa en ny konfigurationsfil från template-filen
    for ort in orter:
        conf_data = create_config_file(TEMPLATE_FILE, ort)
        if conf_data:
            with open(CONF_FOLDER + "/" + ort.split("/")[-1] + ".conf", "w") as fp:
                fp.write(conf_data)

